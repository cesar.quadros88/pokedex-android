package com.example.pokedex.presentation.pokemons

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.databinding.ActivityPokenmonsBinding
import com.example.pokedex.presentation.details.PokemonDetailsActivity

class PokemonsActivity : AppCompatActivity() {

    lateinit var binding: ActivityPokenmonsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPokenmonsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


        val viewModel: PokemonViewModel = ViewModelProvider(this).get(PokemonViewModel::class.java)

        viewModel.pokemonsLiveData.observe(this) { pokemons ->
            with(binding.recyclePokemons){
                layoutManager = LinearLayoutManager(this@PokemonsActivity, RecyclerView.VERTICAL, false)
                setHasFixedSize(true)
                adapter = PokemonAdapter(pokemons) { pokemon ->
                    val intent = PokemonDetailsActivity.getStartIntent(this@PokemonsActivity, pokemon)
                    this@PokemonsActivity.startActivity(intent)
                }
            }
        }
        viewModel.getPokemons()
    }
}