package com.example.pokedex.presentation.pokemons

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.R
import com.example.pokedex.data.ApiService
import com.example.pokedex.data.model.Pokemon
import com.example.pokedex.data.response.PokedexDetailsResponse
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokemonAdapter(
    private val pokemons: List<Pokemon>,
    val onItemClickListener: ((Pokemon: Pokemon) -> Unit)
) : RecyclerView.Adapter<PokemonAdapter.PokemonViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
        return PokemonViewHolder(view, onItemClickListener)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        holder.bindView(pokemons[position])
    }

    override fun getItemCount() = pokemons.count()

    class PokemonViewHolder(
        itemView: View,
        private val onItemClickListener: ((Pokemon: Pokemon) -> Unit)) : RecyclerView.ViewHolder(itemView){
        private val name = itemView.findViewById<TextView>(R.id.txt_name)
        private val imgView = itemView.findViewById<ImageView>(R.id.imgDetails)

        fun bindView(pokemon: Pokemon){
            name.text = pokemon.name

            ApiService.service.getDetails(pokemon.detailsUrl).enqueue(object :
                Callback<PokedexDetailsResponse> {
                override fun onResponse(call: Call<PokedexDetailsResponse>,response: Response<PokedexDetailsResponse>){
                    response.body()?.let { responseDetails ->
                        pokemon.height = responseDetails.height
                        Picasso.get().load(responseDetails.sprites.frontDefault).into(imgView)
                    }
                }
                override fun onFailure(call: Call<PokedexDetailsResponse>, t: Throwable) {
                    Log.e("SPRITE_ERROR", t.message.toString())
                }
            })

            itemView.setOnClickListener{
                onItemClickListener.invoke(pokemon)
            }
        }
    }
}