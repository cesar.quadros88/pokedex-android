package com.example.pokedex.presentation.details

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.pokedex.data.ApiService
import com.example.pokedex.data.model.Pokemon
import com.example.pokedex.data.response.PokedexDetailsResponse
import com.example.pokedex.databinding.ActivityPokemonDetailsBinding
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokemonDetailsActivity : AppCompatActivity() {

    lateinit var binding: ActivityPokemonDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPokemonDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val pokemon = intent.getSerializableExtra("POKEMON") as? Pokemon

        binding.txtDetailsName.text = "Name: " + pokemon!!.name

        ApiService.service.getDetails(pokemon.detailsUrl).enqueue(object :
            Callback<PokedexDetailsResponse> {
            override fun onResponse(call: Call<PokedexDetailsResponse>, response: Response<PokedexDetailsResponse>){
                response.body()?.let { responseDetails ->
                    binding.txtDetailsHeight.text = "Height: " + responseDetails.height
                    binding.txtDetailsWeight.text = "Weight: "+responseDetails.weight
                    Picasso.get().load(responseDetails.sprites.other.home.frontDefault).into(binding.imgDetails)
                }
            }
            override fun onFailure(call: Call<PokedexDetailsResponse>, t: Throwable) {
                Log.e("SPRITE_ERROR", t.message.toString())
            }
        })
    }

    companion object {
        fun getStartIntent(context: Context, pokemon: Pokemon): Intent{
            return Intent(context, PokemonDetailsActivity::class.java).apply {
                putExtra("POKEMON", pokemon)
            }
        }
    }
}