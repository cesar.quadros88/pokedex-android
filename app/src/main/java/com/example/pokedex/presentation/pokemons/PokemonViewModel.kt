package com.example.pokedex.presentation.pokemons

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pokedex.data.ApiService
import com.example.pokedex.data.model.Pokemon
import com.example.pokedex.data.response.PokedexBodyResponse
import com.example.pokedex.data.response.PokedexDetailsResponse
import com.example.pokedex.data.response.PokedexResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokemonViewModel : ViewModel() {

    val pokemonsLiveData: MutableLiveData<List<Pokemon>> = MutableLiveData()

    fun getPokemons(){

        ApiService.service.getPokemons().enqueue(object : Callback<PokedexBodyResponse>{
            override fun onResponse(
                call: Call<PokedexBodyResponse>,
                response: Response<PokedexBodyResponse>
            ) {
                val pokemons: MutableList<Pokemon> = mutableListOf()
                response.body()?.let { pokedexResponse ->
                    for (result in pokedexResponse.pokemonResults){
                        val pokemon = Pokemon(name = result.name, detailsUrl = result.url)
                        pokemons.add(pokemon)
                    }
                }
                pokemonsLiveData.value = pokemons.sortedBy { p -> p.name}
            }

            override fun onFailure(call: Call<PokedexBodyResponse>, t: Throwable) {
                Log.i("LOG", t.message.toString())
            }
        })
    }
}
