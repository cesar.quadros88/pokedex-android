package com.example.pokedex.data.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PokedexDetailsResponse(
    val name: String,
    val height: String,
    val weight: String,
    val sprites : Sprites
)
