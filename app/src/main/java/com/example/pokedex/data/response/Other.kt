package com.example.pokedex.data.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Other(
    val home: Home
)
