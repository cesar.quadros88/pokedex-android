package com.example.pokedex.data.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class Sprites(
    @Json(name = "front_default")
    val frontDefault: String,
    val other: Other
)
