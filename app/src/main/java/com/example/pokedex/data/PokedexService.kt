package com.example.pokedex.data

import com.example.pokedex.data.response.PokedexBodyResponse
import com.example.pokedex.data.response.PokedexDetailsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface PokedexService {
    @GET("pokemon/")
    fun getPokemons() : Call<PokedexBodyResponse>;
    @GET
    fun getDetails(@Url url: String) : Call<PokedexDetailsResponse>
}
