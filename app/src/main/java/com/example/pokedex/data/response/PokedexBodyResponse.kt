package com.example.pokedex.data.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PokedexBodyResponse(
    @Json(name = "results")
    val pokemonResults: List<PokedexResponse>
)