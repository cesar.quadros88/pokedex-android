package com.example.pokedex.data.model

import java.io.Serializable

class Pokemon(
    val name: String,
    val detailsUrl: String,
    var urlImage: String = "",
    var height: String = "",
    val weight: String = ""
) : Serializable